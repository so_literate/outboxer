package outboxer

import (
	"context"
	"io"
	"time"
)

const (
	defaultGracefulTimeout         = time.Second * 30
	defaultGracefulShutdownTimeout = time.Minute
	defaultNextJobTimeout          = time.Second * 2
)

// Config of the outboxer.
// Feel free to left fields empty.
type Config struct {
	// Destination of the logger with errors (default nil).
	ErrorLogger io.Writer

	// PreRunFunc it will be called in the Outboxer.Run method in first place.
	PreRunFunc func(ctx context.Context) context.Context

	// Number of the background runners (default: 1).
	RunnersNumber int

	// Cancel signal will be sent to runners after GracefulTimeout (default: 30s).
	GracefulTimeout time.Duration

	// GracefulShutdownTimeout timeout for outboxer graceful shutdown.
	// Outboxer will wait for this time and after that it will just exit (default: 1m).
	GracefulShutdownTimeout time.Duration

	// The next job will be started after this time (default: 2s).
	NextJobTimeout time.Duration

	// The task with cleaner will be run with this period (default: 1h).
	// You can set value below zero to disable cleaner.
	CleanerPeriod time.Duration

	// The task with restarter will be run with this period (default: disabled).
	// You can set value above zero to enable restarter.
	RestarterPeriod time.Duration

	// RestarterFilter filter to prevent some tasks from restarting (default: all).
	RestarterFilter RestarterFilter

	// Tasks will be marked as stuck if it in work longer then this duration (default: 1m).
	StuckTaskAfter time.Duration

	// Middleware are custom handlers that will be called before the task handler.
	// You can use it to log errors from tasks or enrich the context of the task.
	Middlewares []Middleware
}

// ConfigOption config setter used to set some parameters to Config.
type ConfigOption func(*Config)

// ConfigErrorLogger sets your own error logger writer.
func ConfigErrorLogger(w io.Writer) ConfigOption {
	return func(c *Config) { c.ErrorLogger = w }
}

// ConfigPreRunFunc sets your special function as pre run function.
func ConfigPreRunFunc(fn func(ctx context.Context) context.Context) ConfigOption {
	return func(c *Config) { c.PreRunFunc = fn }
}

// ConfigRunnersNumber sets number of the job runners.
func ConfigRunnersNumber(n int) ConfigOption {
	return func(c *Config) { c.RunnersNumber = n }
}

// ConfigGracefulTimeout sets graceful job timeout.
func ConfigGracefulTimeout(d time.Duration) ConfigOption {
	return func(c *Config) { c.GracefulTimeout = d }
}

// ConfigGracefulShutdownTimeout sets graceful job timeout.
func ConfigGracefulShutdownTimeout(d time.Duration) ConfigOption {
	return func(c *Config) { c.GracefulShutdownTimeout = d }
}

// ConfigNextJobTimeout sets next job checker timeout.
func ConfigNextJobTimeout(d time.Duration) ConfigOption {
	return func(c *Config) { c.NextJobTimeout = d }
}

// ConfigCleanerPeriod sets the launch period of the cleaner.
func ConfigCleanerPeriod(d time.Duration) ConfigOption {
	return func(c *Config) { c.CleanerPeriod = d }
}

// ConfigRestarterPeriod sets the launch period of the restarter.
func ConfigRestarterPeriod(d time.Duration) ConfigOption {
	return func(c *Config) { c.RestarterPeriod = d }
}

// ConfigRestarterFilter sets the filter for restarter.
func ConfigRestarterFilter(filter RestarterFilter) ConfigOption {
	return func(c *Config) { c.RestarterFilter = filter }
}

// ConfigStuckTaskAfter sets time duration after that tasks will be marked as stuck.
func ConfigStuckTaskAfter(d time.Duration) ConfigOption {
	return func(c *Config) { c.StuckTaskAfter = d }
}

// ConfigMiddlewares appends new middlewares to the config.
func ConfigMiddlewares(mw ...Middleware) ConfigOption {
	return func(c *Config) { c.Middlewares = append(c.Middlewares, mw...) }
}

// NewConfig returns new config with default values and set options.
func NewConfig(opts ...ConfigOption) *Config {
	conf := &Config{
		ErrorLogger:             nil,
		PreRunFunc:              nil,
		RunnersNumber:           1,
		GracefulTimeout:         defaultGracefulTimeout,
		GracefulShutdownTimeout: defaultGracefulShutdownTimeout,
		NextJobTimeout:          defaultNextJobTimeout,
		CleanerPeriod:           time.Hour,
		RestarterPeriod:         -1,
		RestarterFilter:         nil,
		StuckTaskAfter:          time.Minute,
		Middlewares:             nil,
	}

	for _, opt := range opts {
		opt(conf)
	}

	return conf
}
