package outboxer

import "errors"

var (
	// ErrTaskNotFound returns from 'GetTask' method when non active task not found in storage.
	ErrTaskNotFound = errors.New("task not found")

	// ErrHandlerNotFound returns when handler of task not found.
	ErrHandlerNotFound = errors.New("handler not found")

	// ErrBadJSONObject returns in panic when json.Marshal method can't Marshal data.
	ErrBadJSONObject = errors.New("bad json object")

	// ErrBadTxType returns from storage when you pass wrong type of the transaction.
	ErrBadTxType = errors.New("bad Tx type")

	// ErrTaskWithoutID returns in save storage method when you pass task without id.
	ErrTaskWithoutID = errors.New("task without id")

	// ErrNewTaskWithID returns in create task method when you pass task with id.
	ErrNewTaskWithID = errors.New("new task with id")
)
