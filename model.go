package outboxer

import (
	"encoding/json"
	"fmt"
	"time"
)

// Task is an element of the outboxer it contains full information about job.
type Task struct {
	// Internal ID of the task.
	ID string
	// The task creation time.
	CreatedAt time.Time
	// The time of the last task update.
	UpdatedAt time.Time
	// The name of the task's handler.
	Handler string
	// The task will be run after this time.
	StartAfter time.Time
	// The task has outbox worker right now, or just stuck without worker.
	InWorkAt *time.Time
	// The task was completed.
	DoneAt *time.Time
	// The task was completed with error.
	DoneWithFailAt *time.Time
	// The latest error of the task.
	LastError *string
	// The task will be clear after completion.
	WillClear *time.Duration
	// The time to clear this task.
	ClearAfter *time.Time
	// The task will be repeated after failed completion.
	RepeatOnFail *time.Duration
	// The task will be repeated after successful completion.
	RepeatTask *time.Duration
	// User data of the task.
	Data json.RawMessage
	// A storage can keep only one active task with this name and this field.
	OnlyOneActive bool
}

// NewTask returns new task with default settings.
func NewTask(handler string, startAfter time.Time, data json.RawMessage) *Task {
	return &Task{
		ID:             "",          // It will be set in CreateTask method.
		CreatedAt:      time.Time{}, // It will be set in CreateTask method.
		UpdatedAt:      time.Time{}, // It will be set in CreateTask method.
		Handler:        handler,
		StartAfter:     startAfter,
		InWorkAt:       nil,   // It will be set in GetTaskForWork method.
		DoneAt:         nil,   // It will be set after GetTaskForWork method.
		DoneWithFailAt: nil,   // It will be set after GetTaskForWork method.
		LastError:      nil,   // It will be set after GetTaskForWork method.
		WillClear:      nil,   // It may be set later.
		ClearAfter:     nil,   // It will be set after GetTaskForWork method.
		RepeatOnFail:   nil,   // It may be set later.
		RepeatTask:     nil,   // It may be set later.
		OnlyOneActive:  false, // It may be set later.
		Data:           data,
	}
}

func marshalDataObject(object interface{}) json.RawMessage {
	var data []byte

	if object != nil {
		var err error

		data, err = json.Marshal(object)
		if err != nil {
			panic(fmt.Errorf("%w: %T(%#v): %s", ErrBadJSONObject, object, object, err))
		}
	}

	return data
}

// NewTaskWithObject returns new Task with object encoded to JSON.
// It will be called panic if you pass bad type for json.Marshal func in object argument.
func NewTaskWithObject(handler string, startAfter time.Time, object interface{}) *Task {
	return NewTask(handler, startAfter, marshalDataObject(object))
}

// UnmarshalData parses the JSON-encoded data and stores the result
// in the value pointed to by dst.
func (t *Task) UnmarshalData(dst interface{}) error {
	return json.Unmarshal(t.Data, dst)
}

// WithData takes any type of data and serialized it to JSON.
// If you pass bad type for json.Marshal func the method will call panic.
func (t *Task) WithData(object interface{}) *Task {
	t.Data = marshalDataObject(object)
	return t
}

// WillBeCleared sets field 'WillClear' this means that task will be cleared after completion.
func (t *Task) WillBeCleared(willClearAfter time.Duration) *Task {
	t.WillClear = &willClearAfter
	return t
}

// WillBeRepeatedOnFail sets field 'RepeatOnFail' this means that task will be repeated after failed completion.
func (t *Task) WillBeRepeatedOnFail(repeatAfter time.Duration) *Task {
	t.RepeatOnFail = &repeatAfter
	return t
}

// WillBeRepeated sets field 'RepeatTask' this means that task will be repeated after completion.
func (t *Task) WillBeRepeated(repeatAfter time.Duration) *Task {
	t.RepeatTask = &repeatAfter
	return t
}

// WillBeOnlyOneActive sets field 'OnlyOneActive' = true, this means that the task will be a
// unique ready for execution in the storage.
func (t *Task) WillBeOnlyOneActive() *Task {
	t.OnlyOneActive = true
	return t
}
