package main

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/jackc/pgx/v4/stdlib"
	"gitlab.com/so_literate/graceful"

	"gitlab.com/so_literate/outboxer"
	"gitlab.com/so_literate/outboxer/storage/postgresql"
)

const (
	dsn         = "host=localhost user=postgres password= dbname=postgres port=5432"
	handlerName = "task_handler"
)

func main() {
	conn, err := sql.Open("pgx", dsn)
	if err != nil {
		log.Fatal("sql.Open: ", err)
	}

	storage, err := postgresql.New(conn, postgresql.ConfigWithSchema("outboxer"))
	if err != nil {
		log.Fatal("postgresql.New: ", err)
	}

	obx := outboxer.New(storage)
	obx.RegisterHandler(handlerName, taskHandler)

	// Will create task after run command.
	go createTask(obx)

	graceful.Log = log.Writer()

	if err := graceful.Run(obx); err != nil {
		log.Fatal("graceful.Run: ", err)
	}

	log.Println("graceful ended")
}

func createTask(obx *outboxer.Outboxer) {
	time.Sleep(time.Second)

	data := &taskData{JSONField: "data of the task"}

	task := outboxer.NewTaskWithObject(handlerName, time.Now(), data).
		WillBeRepeated(time.Second).
		WillBeRepeatedOnFail(time.Second).
		WillBeOnlyOneActive() // This will protect you from duplicating tasks.

	if err := obx.CreateTask(context.Background(), task); err != nil {
		log.Fatal("obx.CreateTask: ", err)
	}
}

type taskData struct {
	JSONField string
}

func taskHandler(ctx context.Context, task *outboxer.Task) error {
	var data taskData

	if err := task.UnmarshalData(&data); err != nil {
		return fmt.Errorf("task.UnmarshalData: %w", err)
	}

	log.Print("task handler: ", data.JSONField)

	return nil
}
