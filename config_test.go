package outboxer_test

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"gitlab.com/so_literate/outboxer"
)

func TestNewConfig(t *testing.T) {
	t.Parallel()

	assert := func(want, got *outboxer.Config) {
		t.Helper()

		wantFormatted := fmt.Sprintf("%#v", want)
		gotFormatted := fmt.Sprintf("%#v", got)

		if wantFormatted != gotFormatted {
			t.Errorf("not equals:\n%s\n%s", wantFormatted, gotFormatted)
		}
	}

	assert(
		&outboxer.Config{
			RunnersNumber:           1,
			GracefulTimeout:         time.Second * 30,
			GracefulShutdownTimeout: time.Minute,
			NextJobTimeout:          time.Second * 2,
			CleanerPeriod:           time.Hour,
			StuckTaskAfter:          time.Minute,
			RestarterPeriod:         -1,
			RestarterFilter:         nil,
		},
		outboxer.NewConfig(),
	)

	preRun := func(ctx context.Context) context.Context {
		return ctx
	}

	mw := func(next outboxer.Handler) outboxer.Handler {
		return func(ctx context.Context, task *outboxer.Task) error {
			return next(ctx, task)
		}
	}

	restarterFilter := func(*outboxer.Task) bool {
		return false
	}

	assert(
		&outboxer.Config{
			ErrorLogger:             os.Stdout,
			PreRunFunc:              preRun,
			RunnersNumber:           10,
			GracefulTimeout:         time.Second,
			GracefulShutdownTimeout: time.Second * 2,
			NextJobTimeout:          time.Second * 10,
			CleanerPeriod:           time.Minute,
			StuckTaskAfter:          time.Hour * 10,
			RestarterPeriod:         time.Hour,
			RestarterFilter:         restarterFilter,
			Middlewares:             []outboxer.Middleware{mw},
		},
		outboxer.NewConfig(
			outboxer.ConfigErrorLogger(os.Stdout),
			outboxer.ConfigPreRunFunc(preRun),
			outboxer.ConfigRunnersNumber(10),
			outboxer.ConfigGracefulTimeout(time.Second),
			outboxer.ConfigGracefulShutdownTimeout(time.Second*2),
			outboxer.ConfigNextJobTimeout(time.Second*10),
			outboxer.ConfigCleanerPeriod(time.Minute),
			outboxer.ConfigStuckTaskAfter(time.Hour*10),
			outboxer.ConfigRestarterPeriod(time.Hour),
			outboxer.ConfigRestarterFilter(restarterFilter),
			outboxer.ConfigMiddlewares(mw),
		),
	)
}
