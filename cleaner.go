package outboxer

import (
	"context"
	"fmt"
	"time"
)

// CleanerHandlerName is a handler name of the cleaner's task.
const CleanerHandlerName = "_outboxer.cleaner"

func (o *Outboxer) cleanerHanlder(ctx context.Context, task *Task) error {
	if err := o.Storage.ClearTasks(ctx, time.Now()); err != nil {
		return fmt.Errorf("failed to clear tasks: %w", err)
	}

	// It was the latest cleaner task.
	if o.config.CleanerPeriod <= 0 {
		task.RepeatTask = nil

		var rightNow time.Duration
		task.WillClear = &rightNow
	}

	return nil
}

func (o *Outboxer) cleanerCreateTask(ctx context.Context) error {
	o.RegisterHandler(CleanerHandlerName, o.cleanerHanlder)

	if o.config.CleanerPeriod <= 0 {
		return nil
	}

	task := NewTask(CleanerHandlerName, time.Now().Add(o.config.CleanerPeriod), nil).
		WillBeRepeatedOnFail(o.config.CleanerPeriod).
		WillBeRepeated(o.config.CleanerPeriod).
		WillBeOnlyOneActive()

	err := o.Storage.CreateTask(ctx, task)
	if err != nil {
		return fmt.Errorf("failed to save the cleaner task in storage: %w", err)
	}

	return nil
}
