package outboxer_test

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"strings"
	"testing"
	"time"

	"gitlab.com/so_literate/outboxer"
)

const handlerName = "handler"

func TestOutboxer_Positive(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithCancel(context.Background())
	testTask := outboxer.NewTask(handlerName, time.Now(), nil)

	storage := NewStorage_Mock(t)

	// Once in start.
	storage.On_Migrate().Rets(nil)

	// Once in start.
	storage.On_CreateTask().Arg_task_Matcher(func(matchTask *outboxer.Task) bool {
		return matchTask.Handler == outboxer.CleanerHandlerName
	}).Rets(nil)

	// Attempts to find jobs.
	storage.On_GetTaskForWork().Rets(testTask, nil).Times(2)

	// Save after work.
	storage.On_SaveTask().Arg_task_Matcher(func(matchTask *outboxer.Task) bool {
		return matchTask.Handler == handlerName
	}).Rets(nil).Times(2)

	var calledHandler bool

	handler := func(ctx context.Context, task *outboxer.Task) error {
		t.Helper()

		if task.Handler != handlerName {
			t.Fatal("wrong task in work", task.Handler)
		}

		if calledHandler {
			cancel() // Stop outboxer on the second task.
		}

		calledHandler = true

		return nil
	}

	obx := outboxer.New(storage,
		outboxer.ConfigNextJobTimeout(time.Millisecond),
		outboxer.ConfigRunnersNumber(1),
	)

	obx.RegisterHandler(handlerName, handler)

	if err := obx.Run(ctx); err != nil {
		t.Fatal("error in obx.Run", err)
	}

	if !calledHandler {
		t.Fatal("not called")
	}

	if err := storage.Mock.AssertExpectations(); err != nil {
		t.Fatal(err)
	}

	if testTask.DoneAt == nil {
		t.Fatal("task is not done")
	}
}

func TestOutboxer_Negative(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithCancel(context.Background())
	testTask := outboxer.NewTask(handlerName, time.Now(), nil)

	storage := NewStorage_Mock(t)
	storage.On_Migrate().Rets(nil)
	storage.On_GetTaskForWork().Rets(testTask, nil).Times(2)
	storage.On_SaveTask().Arg_task_Matcher(func(matchTask *outboxer.Task) bool {
		return matchTask.Handler == handlerName
	}).Rets(nil).Times(2)

	obx := outboxer.New(storage,
		outboxer.ConfigNextJobTimeout(time.Millisecond),
		outboxer.ConfigRunnersNumber(1),
		outboxer.ConfigCleanerPeriod(-1), // No cleaner here.
	)

	var calledHandler bool

	handler := func(ctx context.Context, task *outboxer.Task) error {
		t.Helper()

		if task.Handler != handlerName {
			t.Fatal("wrong task in work", task.Handler)
		}

		if calledHandler {
			cancel() // Stop outboxer on the second task.
			// and return an error.
			return fmt.Errorf("%w: just fake error", outboxer.ErrBadTxType)
		}

		calledHandler = true

		// no error in first time.
		return nil
	}

	obx.RegisterHandler(handlerName, handler)

	if err := obx.Run(ctx); err != nil {
		t.Fatal("error in obx.Run", err)
	}

	if !calledHandler {
		t.Fatal("not called")
	}

	if err := storage.Mock.AssertExpectations(); err != nil {
		t.Fatal(err)
	}

	if testTask.DoneAt != nil {
		t.Fatal("task done successful")
	}

	if testTask.DoneWithFailAt == nil {
		t.Fatal("task is not done with fail")
	}

	if testTask.LastError == nil {
		t.Fatal("task is done without error text")
	}
}

func TestOutboxer_Graceful(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithCancel(context.Background())
	logger := bytes.Buffer{}
	testTask := outboxer.NewTask(handlerName, time.Now(), nil)

	storage := NewStorage_Mock(t)
	storage.On_Migrate().Rets(nil)
	storage.On_GetTaskForWork().Rets(testTask, nil)

	obx := outboxer.New(
		storage,
		outboxer.ConfigErrorLogger(&logger),
		outboxer.ConfigNextJobTimeout(time.Millisecond),
		outboxer.ConfigRunnersNumber(1),
		outboxer.ConfigCleanerPeriod(-1), // No cleaner here.
		outboxer.ConfigGracefulShutdownTimeout(time.Millisecond*100),
	)

	// Will be waiting cancel of the context and will return result.
	handler := func(_ context.Context, _ *outboxer.Task) error {
		cancel()                           // Cancel the parent context of the outboxer.
		time.Sleep(time.Millisecond * 200) // Will sleep longer then GracefulTimeout.

		return nil
	}

	obx.RegisterHandler(handlerName, handler)

	if err := obx.Run(ctx); err != nil {
		t.Fatal("obx.Run:", err)
	}

	if err := storage.Mock.AssertExpectations(); err != nil {
		t.Fatal("storage.Mock.AssertExpectations:", err)
	}

	if !strings.Contains(logger.String(), "GracefulShutdownTimeout reached the deadline") {
		t.Fatalf("wrong log:\n%s", logger.String())
	}
}

func TestOutboxer_TaskGracefulDeadline(t *testing.T) {
	t.Parallel()

	mainCtx, mainCancel := context.WithCancel(context.Background())

	testTask := outboxer.NewTask(handlerName, time.Now(), nil)

	storage := NewStorage_Mock(t)
	storage.On_Migrate().Rets(nil)
	storage.On_GetTaskForWork().Rets(testTask, nil)

	obx := outboxer.New(
		storage,
		outboxer.ConfigNextJobTimeout(time.Millisecond),
		outboxer.ConfigRunnersNumber(1),
		outboxer.ConfigCleanerPeriod(-1),                     // No cleaner here.
		outboxer.ConfigGracefulShutdownTimeout(time.Second),  // waiting for 1 second for outboxer graceful shutdown.
		outboxer.ConfigGracefulTimeout(time.Millisecond*100), // waiting 100 ms for a task.
	)

	taskCtxDone := make(chan struct{})

	// Will be waiting cancel of the context and will return result.
	handler := func(ctx context.Context, _ *outboxer.Task) error {
		mainCancel() // Cancel the main context to provoke graceful task timeout.
		<-ctx.Done() // waiting for canceling task context (ConfigGracefulTimeout).
		close(taskCtxDone)
		return nil
	}

	obx.RegisterHandler(handlerName, handler)

	// outboxer must save the task with error about GracefulTimeout.
	storage.On_SaveTask().Arg_task_Matcher(func(task *outboxer.Task) bool {
		return task.LastError != nil &&
			*task.LastError == fmt.Sprintf("%s: GracefulTimeout reached the deadline for task", context.Canceled)
	})

	if err := obx.Run(mainCtx); err != nil {
		t.Fatal("obx.Run:", err)
	}

	<-taskCtxDone // check that task context is canceled.

	if err := storage.Mock.AssertExpectations(); err != nil {
		t.Fatal("storage.Mock.AssertExpectations:", err)
	}
}

func TestOutboxer_Job(t *testing.T) {
	t.Parallel()

	storage := NewStorage_Mock(t)

	testCase := func(ctx context.Context, handler outboxer.Handler, opts ...outboxer.ConfigOption) {
		t.Helper()

		storage.On_Migrate().Rets(nil) //  always migrate on start

		opts = append(
			opts,
			outboxer.ConfigNextJobTimeout(time.Millisecond),
			outboxer.ConfigRunnersNumber(1),
			outboxer.ConfigCleanerPeriod(-1), // No cleaner here.
		)

		obx := outboxer.New(storage, opts...)

		obx.RegisterHandler(handlerName, handler)

		if err := obx.Run(ctx); err != nil {
			t.Fatal("obx.Run", err)
		}

		if err := storage.Mock.AssertExpectations(); err != nil {
			t.Fatal("storage.Mock.AssertExpectations:", err)
		}
	}

	ctx, cancel := context.WithCancel(context.Background())

	handler := func(_ context.Context, _ *outboxer.Task) error {
		cancel()
		return nil
	}

	// Repeated task.
	now := time.Now()
	testTask := outboxer.NewTask(handlerName, now.Add(-time.Hour), nil).WillBeRepeated(time.Minute)
	storage.On_GetTaskForWork().Rets(testTask, nil)
	storage.On_SaveTask().Arg_task(testTask)

	testCase(ctx, handler)

	if testTask.DoneAt != nil || testTask.InWorkAt != nil {
		t.Fatalf("something wrong with task: %#v", testTask)
	}

	if !testTask.StartAfter.Equal(now.Add(time.Minute)) { // Task must start in the feature to avoid extra runnings.
		t.Fatalf("next StartAfter is wrong: %s", testTask.StartAfter)
	}

	// Task will be removed.
	ctx, cancel = context.WithCancel(context.Background())
	testTask = outboxer.NewTask(handlerName, time.Now(), nil).WillBeCleared(time.Minute)
	storage.On_GetTaskForWork().Rets(testTask, nil)
	storage.On_SaveTask().Arg_task(testTask)

	testCase(ctx, handler)

	if testTask.DoneAt == nil || testTask.ClearAfter.Before(time.Now()) {
		t.Fatalf("something wrong with task: %#v", testTask)
	}

	// Repeated task with error.
	ctx, cancel = context.WithCancel(context.Background())
	handler = func(_ context.Context, _ *outboxer.Task) error {
		cancel()
		return fmt.Errorf("%w: fake test error", outboxer.ErrBadTxType)
	}

	testTask = outboxer.NewTask(handlerName, time.Now(), nil).WillBeRepeatedOnFail(time.Minute)
	storage.On_GetTaskForWork().Rets(testTask, nil)
	storage.On_SaveTask().Arg_task(testTask)

	testCase(ctx, handler)

	if testTask.DoneAt != nil || testTask.DoneWithFailAt != nil {
		t.Fatalf("something wrong with task: %#v", testTask)
	}

	// Middleware: checks that middleware able to change the context of the task.
	mwCalled := false
	ctxSet := false

	type ctxKey int
	const ctxKeyTaskHandler ctxKey = 1

	mw := func(next outboxer.Handler) outboxer.Handler {
		return func(ctx context.Context, task *outboxer.Task) error {
			mwCalled = true
			ctx = context.WithValue(ctx, ctxKeyTaskHandler, task.Handler)
			return next(ctx, task)
		}
	}

	ctx, cancel = context.WithCancel(context.Background())
	handler = func(ctx context.Context, task *outboxer.Task) error {
		taskHandler, ok := ctx.Value(ctxKeyTaskHandler).(string)

		if ok && taskHandler == task.Handler {
			ctxSet = true
		}

		cancel()
		return nil
	}

	testTask = outboxer.NewTask(handlerName, time.Now(), nil)
	storage.On_GetTaskForWork().Rets(testTask, nil)
	storage.On_SaveTask().Arg_task(testTask)

	testCase(ctx, handler, outboxer.ConfigMiddlewares(mw))

	if !mwCalled || !ctxSet {
		t.Fatalf("something wrong with mw: (mwCalled %v, ctxSet %v)", mwCalled, ctxSet)
	}

	// Cleaner task without config.
	ctx, cancel = context.WithCancel(context.Background())
	testTask = outboxer.NewTask(outboxer.CleanerHandlerName, time.Now(), nil)
	storage.On_GetTaskForWork().Rets(testTask, nil)
	stCall := storage.On_SaveTask().Arg_task(testTask)
	stCall.Call.RunFn = func(_ []interface{}) { cancel() }
	storage.On_ClearTasks().Rets(nil)

	testCase(ctx, nil)

	// PreRun func.
	ctx, cancel = context.WithCancel(context.Background())
	testCase(ctx, nil, outboxer.ConfigPreRunFunc(func(ctx context.Context) context.Context {
		cancel()
		return ctx
	}))

	// You can change StartAfter field in the handler.
	now = time.Now()
	nextStart := now.Add(time.Hour)
	ctx, cancel = context.WithCancel(context.Background())

	handler = func(_ context.Context, task *outboxer.Task) error {
		task.StartAfter = nextStart
		cancel()
		return nil
	}

	testTask = outboxer.NewTask(handlerName, now, nil).WillBeRepeated(time.Minute)
	storage.On_GetTaskForWork().Rets(testTask, nil)
	storage.On_SaveTask().Arg_task(testTask)

	testCase(ctx, handler)

	if !testTask.StartAfter.Equal(nextStart) {
		t.Fatalf("next StartAfter is wrong: %s", testTask.StartAfter)
	}
}

func TestOutboxer_Wrappers(t *testing.T) {
	t.Parallel()

	storage := NewStorage_Mock(t)
	obx := outboxer.New(storage)

	task := obx.NewTask(handlerName, time.Now(), nil)

	if task.Handler != handlerName {
		t.Fatal(task.Handler)
	}

	ctx := context.Background()

	storage.On_CreateTask().Args(ctx, task).Rets(nil)
	if err := obx.CreateTask(ctx, task); err != nil {
		t.Fatal(err)
	}

	storage.On_CreateTaskWithTx().Args(ctx, nil, task).Rets(nil)
	if err := obx.CreateTaskWithTx(ctx, nil, task); err != nil {
		t.Fatal(err)
	}

	var tx *sql.Tx
	storage.On_CreateTaskWithTx().Args(ctx, tx, task).Rets(nil)
	if err := obx.CreateTaskWithTxSQL(ctx, tx, task); err != nil {
		t.Fatal(err)
	}

	storage.On_GetProblemTasks().Rets(1, 2, nil)
	stuck, failed, err := obx.GetProblemTasksAmount(ctx)
	if err != nil {
		t.Fatal(err)
	}

	if stuck != 1 {
		t.Fatal(stuck)
	}

	if failed != 2 {
		t.Fatal(failed)
	}

	if err := storage.Mock.AssertExpectations(); err != nil {
		t.Fatal(err)
	}
}

func TestOutboxer_Restarter(t *testing.T) {
	t.Parallel()

	ctx, cancel := context.WithCancel(context.Background())
	storage := NewStorage_Mock(t)

	defer func() {
		if err := storage.Mock.AssertExpectations(); err != nil {
			t.Fatal(err)
		}
	}()

	// Once in start.
	storage.On_Migrate().Rets(nil)

	// Once in start.
	storage.On_CreateTask().Arg_task_Matcher(func(matchTask *outboxer.Task) bool {
		return matchTask.Handler == outboxer.RestarterHandlerName
	}).Rets(nil)

	obx := outboxer.New(
		storage,
		outboxer.ConfigNextJobTimeout(time.Hour),
		outboxer.ConfigCleanerPeriod(-1),
		outboxer.ConfigRestarterPeriod(time.Second),
		outboxer.ConfigRestarterFilter(func(task *outboxer.Task) bool { return task.ID != "2" }),
	)

	obxDone := make(chan error, 1)

	go func() {
		obxDone <- obx.Run(ctx)
	}()

	now := time.Now()
	hourAgo := now.Add(-time.Hour)

	tasks := []*outboxer.Task{
		{ID: "1", InWorkAt: &now, StartAfter: hourAgo},
		{ID: "2", InWorkAt: &now, StartAfter: hourAgo},
		{ID: "3", InWorkAt: &now, StartAfter: hourAgo},
	}

	// Prepare the Storage.
	storage.On_GetStuckTasks().Arg_ctx(ctx).Rets(tasks, nil)

	storage.On_SaveTask().Arg_ctx(ctx).Arg_task_Matcher(func(task *outboxer.Task) bool {
		return task.ID == "1" && task.InWorkAt == nil && task.StartAfter.After(time.Now())
	})

	storage.On_SaveTask().Arg_ctx(ctx).Arg_task_Matcher(func(task *outboxer.Task) bool {
		return task.ID == "3" && task.InWorkAt == nil && task.StartAfter.After(time.Now())
	})

	err := obx.RestartStuckTasks(ctx)
	if err != nil {
		t.Fatal(err)
	}

	cancel()

	err = <-obxDone
	if err != nil {
		t.Fatal(err)
	}
}
