package outboxer

import (
	"context"
	"fmt"
	"time"
)

// RestarterHandlerName is a handler name of the restarter's task.
// This task finds stuck tasks and restarts them.
const RestarterHandlerName = "_outboxer.restarter"

// RestarterFilter is a filter to prevent some tasks from restarting.
type RestarterFilter func(task *Task) bool

func (o *Outboxer) restarterHandler(ctx context.Context, task *Task) error {
	tasks, err := o.Storage.GetStuckTasks(ctx, time.Now().Add(-o.config.StuckTaskAfter))
	if err != nil {
		return fmt.Errorf("storage.GetStuckTasks: %w", err)
	}

	if o.config.RestarterFilter != nil {
		n := 0
		for _, t := range tasks {
			if o.config.RestarterFilter(t) {
				tasks[n] = t
				n++
			}
		}

		tasks = tasks[:n]
	}

	restartTime := time.Now()

	for _, t := range tasks {
		restartTime = restartTime.Add(time.Minute)

		t.InWorkAt = nil
		t.StartAfter = restartTime

		err = o.Storage.SaveTask(ctx, t)
		if err != nil {
			return fmt.Errorf("storage.SaveTask (%s): %w", task.Handler, err)
		}
	}

	if o.config.RestarterPeriod <= 0 { // It was the latest restarter's task.
		task.RepeatTask = nil

		var rightNow time.Duration
		task.WillClear = &rightNow
		return nil
	}

	return nil
}

func (o *Outboxer) restarterCreateTask(ctx context.Context) error {
	o.RegisterHandler(RestarterHandlerName, o.restarterHandler)

	if o.config.RestarterPeriod <= 0 {
		return nil
	}

	task := NewTask(RestarterHandlerName, time.Now().Add(o.config.RestarterPeriod), nil).
		WillBeRepeatedOnFail(o.config.RestarterPeriod).
		WillBeRepeated(o.config.RestarterPeriod).
		WillBeOnlyOneActive()

	err := o.Storage.CreateTask(ctx, task)
	if err != nil {
		return fmt.Errorf("failed to save the restarter task in storage: %w", err)
	}

	return nil
}
