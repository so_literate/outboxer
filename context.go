package outboxer

import (
	"context"
	"time"
)

// contextWithoutCancel is a context without propagateCancel.
//
//nolint:containedctx // it's no canceled context.
type contextWithoutCancel struct {
	ctx context.Context
}

func (c contextWithoutCancel) Deadline() (time.Time, bool)       { return time.Time{}, false }
func (c contextWithoutCancel) Done() <-chan struct{}             { return nil }
func (c contextWithoutCancel) Err() error                        { return nil }
func (c contextWithoutCancel) Value(key interface{}) interface{} { return c.ctx.Value(key) }

// NewContextWithoutCancel creates new context that will ignore cancel signal at all.
func NewContextWithoutCancel(ctx context.Context) context.Context {
	return contextWithoutCancel{ctx: ctx}
}
