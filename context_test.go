package outboxer_test

import (
	"context"
	"errors"
	"testing"

	"gitlab.com/so_literate/outboxer"
)

func TestNewContextWithoutCancel(t *testing.T) {
	t.Parallel()

	type ctxKey struct{}
	value := "text"

	parent, cancel := context.WithCancel(context.WithValue(context.Background(), ctxKey{}, value))

	noCancel := outboxer.NewContextWithoutCancel(parent)

	// It keeps our values.
	res := noCancel.Value(ctxKey{})
	if res != value {
		t.Fatal("wrong value:", value)
	}

	cancel()
	<-parent.Done()

	if err := parent.Err(); err == nil {
		t.Fatal("no error in canceled ctx")
	}

	// It doesn't know about canceled parent.
	if noCancel.Done() != nil {
		t.Fatal("noCancel is canceled to")
	}

	deadline, ok := noCancel.Deadline()
	if ok {
		t.Fatal("ctx with deadline")
	}

	if !deadline.IsZero() {
		t.Fatal("deadline is not zero")
	}

	if err := noCancel.Err(); err != nil {
		t.Fatal(err)
	}
}

func TestContextWithoutCancel_propagateCancelling(t *testing.T) {
	t.Parallel()

	type ctxKey int
	const ctxKeyTest ctxKey = 1

	ctx := context.WithValue(context.Background(), ctxKeyTest, "value")

	var bgCancel func()
	ctx, bgCancel = context.WithCancel(ctx)

	noCancel := outboxer.NewContextWithoutCancel(ctx)

	ctxCancelOfNoCancel, cancelOfNoCancel := context.WithCancel(noCancel)

	// Check that we still have value in the context.
	_, ok := ctxCancelOfNoCancel.Value(ctxKeyTest).(string)
	if !ok {
		t.Fatal("context doesn't have value")
	}

	// cancel BG and check that ctxCancelOfNoCancel still active.
	bgCancel()

	err := ctx.Err()
	if !errors.Is(err, context.Canceled) {
		t.Fatal("2nd child is not canceled")
	}

	err = ctxCancelOfNoCancel.Err()
	if err != nil {
		t.Fatal("2nd child context canceled")
	}

	// cancel 2nd child.
	cancelOfNoCancel()

	err = ctxCancelOfNoCancel.Err()
	if !errors.Is(err, context.Canceled) {
		t.Fatal("2nd child is not canceled")
	}
}
