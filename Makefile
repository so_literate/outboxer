test:
	go test -cover ./...

coverage:
	go test -coverpkg=./... -coverprofile=profile.cov ./...
	go tool cover -func profile.cov

	# last line will be number of total coverage
	go tool cover -func profile.cov | tail -n1 | awk '{print $$3}'

test_int_local:
	cd ./storage/postgresql/integration &&\
	OUTBOXER_DSN="host=localhost user=gotest password=gotest dbname=outboxer port=5432" go test -v ./...

test_int_ci:
	cd ./storage/postgresql/integration && go test -v ./...

lints:
	golangci-lint version
	golangci-lint run -c ./.linters.yml

lints_fix:
	golangci-lint run --fix -c ./.linters.yml
