package integration_test

import (
	"database/sql"
	"fmt"
	"os"
	"testing"

	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/stretchr/testify/require"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/so_literate/outboxer/storage/postgresql"
)

type testHelper struct {
	schema    string
	tableName string
	conn      *sql.DB

	t  *testing.T
	so *require.Assertions
}

const (
	outboxerDsnEnv = "OUTBOXER_DSN"
	skipReason     = "Database dsn is empty"
)

func newSQLStorage(t *testing.T, schema, tableName string, isCleanable bool) (*testHelper, *postgresql.Storage) {
	t.Helper()

	dsn := os.Getenv(outboxerDsnEnv)
	if dsn == "" {
		t.Skip(skipReason)
	}

	so := require.New(t)

	conn, err := sql.Open("pgx", dsn)
	so.NoError(err)

	h := &testHelper{
		schema:    schema,
		tableName: tableName,
		conn:      conn,
		t:         t,
		so:        so,
	}

	storage := h.newStorage(isCleanable)

	return h, storage
}

func newGormStorage(t *testing.T, schema, tableName string, isCleanable bool) (*testHelper, *postgresql.Storage, *gorm.DB) {
	t.Helper()

	dsn := os.Getenv(outboxerDsnEnv)
	if dsn == "" {
		t.Skip(skipReason)
	}

	so := require.New(t)

	db, err := gorm.Open(postgres.Open(dsn))
	so.NoError(err)

	conn, err := db.DB()
	so.NoError(err)

	h := &testHelper{
		schema:    schema,
		tableName: tableName,
		conn:      conn,
		t:         t,
		so:        so,
	}

	storage := h.newStorage(isCleanable)

	return h, storage, db
}

func (h *testHelper) newStorage(isCleanable bool) *postgresql.Storage {
	h.t.Helper()

	storage, err := postgresql.New(
		h.conn,
		postgresql.ConfigWithSchema(h.schema),
		postgresql.ConfigWithTableName(h.tableName),
	)
	h.so.NoError(err)
	h.so.NotNil(storage)

	h.prepareScheme(isCleanable)

	return storage
}

func (h *testHelper) prepareScheme(isCleanable bool) {
	h.t.Helper()

	_, err := h.conn.Exec(fmt.Sprintf(`DROP SCHEMA IF EXISTS %s CASCADE;`, h.schema))
	h.so.NoError(err)

	if isCleanable {
		h.t.Cleanup(func() {
			_, errDrop := h.conn.Exec(fmt.Sprintf(`DROP SCHEMA %s CASCADE;`, h.schema))
			h.so.NoError(errDrop)
		})
	}
}

func (h *testHelper) checkRows(wantRows int64, where string) {
	h.t.Helper()

	query := fmt.Sprintf(`SELECT count(1) FROM %q.%q `, h.schema, h.tableName)

	if where != "" {
		query += "WHERE " + where + ";"
	} else {
		query += ";"
	}

	var rows int64
	err := h.conn.QueryRow(query).Scan(&rows)

	h.so.NoError(err)
	h.so.Equalf(wantRows, rows, "rows in database: where %q", where)
}
