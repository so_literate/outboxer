package integration_test

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gorm.io/gorm"

	"gitlab.com/so_literate/outboxer"
	"gitlab.com/so_literate/outboxer/storage/postgresql"
)

const cleanDB = true

func TestStorage(t *testing.T) {
	t.Parallel()
	so := require.New(t)
	ctx := context.Background()

	helper, storage := newSQLStorage(t, "storage", "storage", cleanDB)

	// No error when we try to create schema on empty DB.
	so.NoError(storage.Migrate(ctx))
	// If we will try to migrate again then no error too.
	so.NoError(storage.Migrate(ctx))

	helper.checkRows(0, "")

	dbTask, err := storage.GetTaskForWork(ctx, time.Now())
	so.Nil(dbTask)
	so.ErrorIs(err, outboxer.ErrTaskNotFound)

	task := outboxer.NewTask("handler", time.Now().Add(time.Minute), nil)

	err = storage.CreateTask(ctx, task)
	so.NoError(err)
	so.Equal("1", task.ID)

	helper.checkRows(1, "")

	dbTask, err = storage.GetTaskForWork(ctx, time.Now())
	so.Nil(dbTask)
	so.ErrorIs(err, outboxer.ErrTaskNotFound)

	dbTask, err = storage.GetTaskForWork(ctx, time.Now().Add(time.Minute))
	so.NoError(err)
	so.Equal(task.ID, dbTask.ID)
	so.NotNil(dbTask.InWorkAt)

	now := time.Now()
	dbTask.DoneAt = &now
	err = storage.SaveTask(ctx, dbTask)
	so.NoError(err)

	helper.checkRows(1, "done_at IS NOT NULL")

	errorText := "error"
	dur := time.Minute

	dbTask.DoneWithFailAt = &now
	dbTask.LastError = &errorText
	dbTask.WillClear = &dur
	dbTask.ClearAfter = &now
	dbTask.RepeatOnFail = &dur
	dbTask.RepeatTask = &dur
	dbTask.OnlyOneActive = true
	dbTask.Data = []byte(`"some text"`)

	err = storage.SaveTask(ctx, dbTask)
	so.NoError(err)

	helper.checkRows(1, "done_with_fail_at IS NOT NULL AND last_error      IS NOT NULL")
	helper.checkRows(1, "will_clear        IS NOT NULL AND clear_after     IS NOT NULL")
	helper.checkRows(1, "repeat_on_fail    IS NOT NULL AND repeat_task     IS NOT NULL")
	helper.checkRows(1, "data              IS NOT NULL AND only_one_active = true")

	task = outboxer.NewTask("handler_2", time.Now(), nil)
	err = storage.CreateTask(ctx, task)
	so.NoError(err)
	helper.checkRows(2, "")

	err = storage.ClearTasks(ctx, time.Now())
	so.NoError(err)

	helper.checkRows(1, "")
	helper.checkRows(1, "handler = 'handler_2'")
}

func TestParseFields(t *testing.T) {
	t.Parallel()
	so := require.New(t)
	ctx := context.Background()

	helper, storage := newSQLStorage(t, "parse", "fields", cleanDB)

	err := storage.Migrate(ctx)
	so.NoError(err)

	task := outboxer.NewTaskWithObject("task_handler", time.Now(), "some json data").
		WillBeRepeated(time.Minute).WillBeRepeatedOnFail(time.Second).WillBeCleared(time.Hour)

	err = storage.CreateTask(ctx, task)
	so.NoError(err)
	so.Equal("1", task.ID)

	helper.checkRows(1, "handler = 'task_handler'")

	dbTask, err := storage.GetTaskForWork(ctx, time.Now())
	so.NoError(err)
	so.Equal(task.ID, dbTask.ID)
	so.Equal(time.Minute, *task.RepeatTask)
	so.Equal(time.Second, *task.RepeatOnFail)
	so.Equal(time.Hour, *task.WillClear)
	so.Equal(`"some json data"`, string(task.Data))
}

func TestOnlyOneActive(t *testing.T) {
	t.Parallel()
	so := require.New(t)
	ctx := context.Background()

	helper, storage := newSQLStorage(t, "one", "active", cleanDB)

	err := storage.Migrate(ctx)
	so.NoError(err)

	uniqueHandler := "unique"
	anotherUniqueHandler := "another_unique"

	task := outboxer.NewTask(uniqueHandler, time.Now(), nil).WillBeOnlyOneActive()
	err = storage.CreateTask(ctx, task)
	so.NoError(err)

	helper.checkRows(1, "")

	task = outboxer.NewTask(uniqueHandler, time.Now(), nil).WillBeOnlyOneActive()
	err = storage.CreateTask(ctx, task)
	so.NoError(err)
	so.Equal("", task.ID)

	helper.checkRows(1, "")

	dbTask, err := storage.GetTaskForWork(ctx, time.Now())
	so.NoError(err)
	so.Equal("1", dbTask.ID)

	err = storage.CreateTask(ctx, task)
	so.NoError(err)

	helper.checkRows(1, "")

	now := time.Now()
	dbTask.DoneAt = &now
	err = storage.SaveTask(ctx, dbTask)
	so.NoError(err)

	err = storage.CreateTask(ctx, task)
	so.NoError(err)
	so.Equal("4", task.ID)

	helper.checkRows(2, "")

	task = outboxer.NewTask(uniqueHandler, time.Now(), nil).WillBeOnlyOneActive()
	err = storage.CreateTask(ctx, task)
	so.NoError(err)

	helper.checkRows(2, "")

	task = outboxer.NewTask(anotherUniqueHandler, time.Now(), nil).WillBeOnlyOneActive()
	err = storage.CreateTask(ctx, task)
	so.NoError(err)
	so.Equal("6", task.ID)

	helper.checkRows(3, "")

	task = outboxer.NewTask(anotherUniqueHandler, time.Now(), nil).WillBeOnlyOneActive()
	err = storage.CreateTask(ctx, task)
	so.NoError(err)
	so.Equal("", task.ID)

	helper.checkRows(3, "")
	helper.checkRows(2, "in_work_at IS NULL")
	helper.checkRows(1, "in_work_at IS NOT NULL")
	helper.checkRows(2, "handler = 'unique'")
	helper.checkRows(1, "handler = 'another_unique'")
}

func TestProblemTasks(t *testing.T) {
	t.Parallel()
	so := require.New(t)
	ctx := context.Background()

	helper, storage := newSQLStorage(t, "problem", "tasks", cleanDB)

	err := storage.Migrate(ctx)
	so.NoError(err)

	task := outboxer.NewTask("stuck", time.Now().Add(-time.Hour), nil)
	err = storage.CreateTask(ctx, task)
	so.NoError(err)

	dbTask, err := storage.GetTaskForWork(ctx, time.Now().Add(-time.Minute)) // like we took the task 1 min ago.
	so.NoError(err)

	helper.checkRows(1, "")

	// No one task stuck an hour ago.
	stuck, failed, err := storage.GetProblemTasks(ctx, time.Now().Add(-time.Hour))
	so.NoError(err)
	so.Equal(int64(0), stuck)
	so.Equal(int64(0), failed)

	// But 1 min ago we have one stuck task.
	stuck, failed, err = storage.GetProblemTasks(ctx, time.Now().Add(-time.Minute))
	so.NoError(err)
	so.Equal(int64(1), stuck)
	so.Equal(int64(0), failed)

	now := time.Now()
	dbTask.DoneWithFailAt = &now

	err = storage.SaveTask(ctx, dbTask)
	so.NoError(err)
	helper.checkRows(1, "")

	stuck, failed, err = storage.GetProblemTasks(ctx, time.Now())
	so.NoError(err)
	so.Equal(int64(0), stuck)
	so.Equal(int64(1), failed)
}

func TestGetStuckTasks(t *testing.T) {
	t.Parallel()
	so := require.New(t)
	ctx := context.Background()

	helper, storage := newSQLStorage(t, "stuck", "tasks", cleanDB)

	err := storage.Migrate(ctx)
	so.NoError(err)

	task := outboxer.NewTask("stuck", time.Now().Add(-time.Hour), nil)
	err = storage.CreateTask(ctx, task)
	so.NoError(err)

	dbTask, err := storage.GetTaskForWork(ctx, time.Now().Add(-time.Minute)) // like we took the task 1 min ago.
	so.NoError(err)

	helper.checkRows(1, "")

	// No one task stuck an hour ago.
	tasks, err := storage.GetStuckTasks(ctx, time.Now().Add(-time.Hour))
	so.NoError(err)
	so.Len(tasks, 0)

	// But 1 min ago we have one stuck task.
	tasks, err = storage.GetStuckTasks(ctx, time.Now().Add(-time.Minute))
	so.NoError(err)
	so.Len(tasks, 1)
	so.Equal(dbTask.ID, tasks[0].ID)

	// Reset the task.
	dbTask.InWorkAt = nil
	dbTask.StartAfter = time.Now()

	err = storage.SaveTask(ctx, dbTask)
	so.NoError(err)
	helper.checkRows(1, "")

	tasks, err = storage.GetStuckTasks(ctx, time.Now())
	so.NoError(err)
	so.Len(tasks, 0)
}

func TestTransactions(t *testing.T) {
	t.Parallel()
	so := require.New(t)
	ctx := context.Background()

	helper, storage := newSQLStorage(t, "transactions", "tasks", cleanDB)

	err := storage.Migrate(ctx)
	so.NoError(err)

	// With rollback.
	firstTask := outboxer.NewTask("first", time.Now(), nil)
	secondTask := outboxer.NewTask("second", time.Now(), nil)

	tx, err := helper.conn.BeginTx(ctx, nil)
	so.NoError(err)

	err = storage.CreateTaskWithTx(ctx, tx, firstTask)
	so.NoError(err)
	so.Equal("1", firstTask.ID)

	err = storage.CreateTaskWithTx(ctx, tx, secondTask)
	so.NoError(err)
	so.Equal("2", secondTask.ID)

	err = tx.Rollback()
	so.NoError(err)

	helper.checkRows(0, "")

	// With commit.
	firstTask.ID = ""
	secondTask.ID = ""

	tx, err = helper.conn.BeginTx(ctx, nil)
	so.NoError(err)

	err = storage.CreateTaskWithTx(ctx, tx, firstTask)
	so.NoError(err)
	so.Equal("3", firstTask.ID)

	err = storage.CreateTaskWithTx(ctx, tx, secondTask)
	so.NoError(err)
	so.Equal("4", secondTask.ID)

	err = tx.Commit()
	so.NoError(err)

	helper.checkRows(2, "")
}

type gormTx struct {
	tx *gorm.DB
}

func (g gormTx) QueryRowAndScan(ctx context.Context, query string, args []interface{}, scanID *int64) error {
	return g.tx.WithContext(ctx).Raw(query, args...).Scan(scanID).Error
}

var _ postgresql.RowQuerier = gormTx{}

func TestGormTransactions(t *testing.T) {
	t.Parallel()
	so := require.New(t)
	ctx := context.Background()

	helper, storage, db := newGormStorage(t, "gorm", "transactions", cleanDB)

	err := storage.Migrate(ctx)
	so.NoError(err)

	// With rollback.
	firstTask := outboxer.NewTask("first", time.Now(), nil)
	secondTask := outboxer.NewTask("second", time.Now(), nil)

	err = db.Transaction(func(tx *gorm.DB) error {
		gtx := gormTx{tx: tx}

		err = storage.CreateTaskWithTx(ctx, gtx, firstTask)
		so.NoError(err)
		so.Equal("1", firstTask.ID)

		err = storage.CreateTaskWithTx(ctx, gtx, secondTask)
		so.NoError(err)
		so.Equal("2", secondTask.ID)

		return outboxer.ErrHandlerNotFound // just fake error for rollback
	})
	so.ErrorIs(err, outboxer.ErrHandlerNotFound)
	helper.checkRows(0, "")

	// With commit.
	firstTask.ID = ""
	secondTask.ID = ""

	err = db.Transaction(func(tx *gorm.DB) error {
		gtx := gormTx{tx: tx}

		err = storage.CreateTaskWithTx(ctx, gtx, firstTask)
		so.NoError(err)
		so.Equal("3", firstTask.ID)

		err = storage.CreateTaskWithTx(ctx, gtx, secondTask)
		so.NoError(err)
		so.Equal("4", secondTask.ID)

		return nil
	})
	so.NoError(err)
	helper.checkRows(2, "")

	// With unique task, gorm do not return an error.
	uniqueTask := outboxer.NewTask("unique", time.Now(), nil).WillBeOnlyOneActive()
	err = storage.CreateTaskWithTx(ctx, gormTx{tx: db}, uniqueTask)

	so.NoError(err)
	helper.checkRows(1, "handler = 'unique'")
	so.Equal("5", uniqueTask.ID)

	uniqueTask.ID = "123"
	err = storage.CreateTaskWithTx(ctx, gormTx{tx: db}, uniqueTask)
	so.NoError(err)
	helper.checkRows(1, "handler = 'unique'")
	so.Equal("", uniqueTask.ID)
}
