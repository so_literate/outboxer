// Package postgresql is an implementation of the Outboxer storage.
// It tested with pgx driver, see "integration" package.
package postgresql

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"gitlab.com/so_literate/outboxer"
	"gitlab.com/so_literate/outboxer/storage/postgresql/query"
)

// Storage contains methods to manage tasks in database.
type Storage struct {
	query *query.Query
	conn  *sql.DB
}

var _ outboxer.Storage = (*Storage)(nil)

// New returns a new postgres database storage of the Outboxer.
func New(conn *sql.DB, confOptions ...ConfigSetter) (*Storage, error) {
	conf, err := newConfig().runSetters(confOptions).checkIsValid()
	if err != nil {
		return nil, fmt.Errorf("failed to create config: %w", err)
	}

	s := &Storage{
		query: query.New(conf.Schema, conf.TableName),
		conn:  conn,
	}

	return s, nil
}

func rollback(tx *sql.Tx, originErr error) error {
	if rollbackErr := tx.Rollback(); rollbackErr != nil {
		return fmt.Errorf("%w (tx.Rollback: %s)", originErr, rollbackErr)
	}

	return originErr
}

// Migrate checks current scheme and migrates it if empty.
func (s *Storage) Migrate(ctx context.Context) error {
	var exists bool

	if err := s.conn.QueryRowContext(ctx, s.query.CheckTableExists).Scan(&exists); err != nil {
		return fmt.Errorf("exec CheckTableExists: %w (%s)", err, s.query.CheckTableExists)
	}

	if exists {
		return nil
	}

	tx, err := s.conn.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelSerializable})
	if err != nil {
		return fmt.Errorf("failed to start transactions: %w", err)
	}

	_, err = tx.ExecContext(ctx, s.query.Migrate)
	if err != nil {
		return rollback(tx, fmt.Errorf("exec Migrate: %w (%s)", err, s.query.Migrate))
	}

	if err = tx.Commit(); err != nil {
		return fmt.Errorf("failed to commit tx: %w", err)
	}

	return nil
}

// GetTaskForWork returns non active task on the current time.
func (s *Storage) GetTaskForWork(ctx context.Context, now time.Time) (*outboxer.Task, error) {
	var t task

	err := s.conn.QueryRowContext(ctx, s.query.GetTaskForWork, now).Scan(
		&t.id, &t.createdAt, &t.updatedAt, &t.handler, &t.startAfter,
		&t.inWorkAt, &t.doneAt, &t.doneWithFailAt, &t.lastError,
		&t.willClear, &t.clearAfter, &t.repeatOnFail, &t.repeatTask,
		&t.data, &t.onlyOneActive,
	)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, outboxer.ErrTaskNotFound
		}

		return nil, fmt.Errorf("exec GetTaskForWork: %w (%s)", err, s.query.GetTaskForWork)
	}

	var res outboxer.Task
	t.mapToModel(&res)

	return &res, nil
}

type stdQuerier interface {
	QueryRowContext(ctx context.Context, query string, args ...interface{}) *sql.Row
}

var (
	_ stdQuerier = &sql.Tx{}
	_ stdQuerier = &sql.DB{}
)

type stdRowQuerier struct {
	conn stdQuerier
}

func (q stdRowQuerier) QueryRowAndScan(ctx context.Context, sqlQuery string, args []interface{}, scanID *int64) error {
	return q.conn.QueryRowContext(ctx, sqlQuery, args...).Scan(scanID) //nolint:wrapcheck // no need extra context.
}

// RowQuerier is a flexible interface provides using different ORM as sql transaction.
type RowQuerier interface {
	QueryRowAndScan(ctx context.Context, query string, args []interface{}, scanID *int64) error
}

var _ RowQuerier = stdRowQuerier{}

func (s *Storage) createTask(ctx context.Context, querier RowQuerier, mt *outboxer.Task) error {
	t, err := newTask(mt)
	if err != nil {
		return fmt.Errorf("newTask: %w", err)
	}

	now := time.Now()

	t.id = 0
	t.createdAt = now
	t.updatedAt = now

	err = querier.QueryRowAndScan(ctx, s.query.CreateTask, t.getInsertColumns(), &t.id)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			return fmt.Errorf("exec CreateTask: %w (%s)", err, s.query.CreateTask)
		}
	}

	t.mapToModel(mt)

	return nil
}

// CreateTask creates a new task with, fields: ID, created_at, updated_at will be set.
func (s *Storage) CreateTask(ctx context.Context, task *outboxer.Task) error {
	if task.ID != "" {
		return fmt.Errorf("%w: %q", outboxer.ErrNewTaskWithID, task.ID)
	}

	return s.createTask(ctx, stdRowQuerier{conn: s.conn}, task)
}

// CreateTaskWithTx creates a new task in transaction.
// If you use sqlx or std lib you can pass just *sql.Tx as tx interface{}.
// For Gorm or other project without access to *sql.DB/Tx, you can implement the RowQuerier.
// For example gorm:
//
//	  type gormTx struct {
//	    tx *gorm.DB
//	  }
//	  func (g gormTx) QueryRowAndScan(ctx context.Context, query string, args []interface{}, scanID *int64) error {
//		    return g.tx.WithContext(ctx).Raw(query, args...).Scan(scanID).Error
//	  }
//
// SQL query contain 'ON CONFLICT DO NOTHING RETURNING id', so it must return sql.ErrNoRows or nil
// if you can't scan id of the not created row. Gorm just ignore this error and return nil.
func (s *Storage) CreateTaskWithTx(ctx context.Context, tx interface{}, task *outboxer.Task) error {
	var querier RowQuerier

	switch tp := tx.(type) {
	case stdQuerier:
		querier = stdRowQuerier{conn: tp}

	case RowQuerier:
		querier = tp

	default:
		return fmt.Errorf("%w: want (*sql.Tx/*sql.DB/RowQuerier), got (%T)", outboxer.ErrBadTxType, tx)
	}

	if err := s.createTask(ctx, querier, task); err != nil {
		return fmt.Errorf("createTask: %w", err)
	}

	return nil
}

// SaveTask saves all changes of the task.
func (s *Storage) SaveTask(ctx context.Context, task *outboxer.Task) error {
	if task.ID == "" {
		return outboxer.ErrTaskWithoutID
	}

	task.UpdatedAt = time.Now()

	t, err := newTask(task)
	if err != nil {
		return fmt.Errorf("newTask: %w", err)
	}

	_, err = s.conn.ExecContext(ctx, s.query.SaveTask, t.getUpdateColumns()...)
	if err != nil {
		return fmt.Errorf("exec SaveTask: %w (%s)", err, s.query.SaveTask)
	}

	return nil
}

// ClearTasks clears all tasks with mark 'ClearAfter'.
func (s *Storage) ClearTasks(ctx context.Context, now time.Time) error {
	_, err := s.conn.ExecContext(ctx, s.query.ClearTasks, now)
	if err != nil {
		return fmt.Errorf("exec ClearTasks: %w (%s)", err, s.query.ClearTasks)
	}

	return nil
}

// GetProblemTasks returns number of the stuck or failed tasks without repeat.
func (s *Storage) GetProblemTasks(ctx context.Context, inWorkOlderThan time.Time) (stuck, failed int64, err error) {
	err = s.conn.QueryRowContext(ctx, s.query.GetProblemTasks, inWorkOlderThan).Scan(&stuck, &failed)
	if err != nil {
		return 0, 0, fmt.Errorf("exec GetProblemTasks: %w (%s)", err, s.query.GetProblemTasks)
	}

	return stuck, failed, nil
}

// GetStuckTasks returns stuck tasks from storage used by restarter.
func (s *Storage) GetStuckTasks(ctx context.Context, inWorkOlderThan time.Time) ([]*outboxer.Task, error) {
	rows, err := s.conn.QueryContext(ctx, s.query.GetStuckTasks, inWorkOlderThan)
	if err != nil {
		return nil, fmt.Errorf("conn.QueryContext: %w", err)
	}

	defer rows.Close() //nolint:errcheck // doesn't matter.

	tasks := make([]*outboxer.Task, 0, 50)

	for rows.Next() {
		var t task
		err = rows.Scan(
			&t.id, &t.createdAt, &t.updatedAt, &t.handler, &t.startAfter,
			&t.inWorkAt, &t.doneAt, &t.doneWithFailAt, &t.lastError,
			&t.willClear, &t.clearAfter, &t.repeatOnFail, &t.repeatTask,
			&t.data, &t.onlyOneActive,
		)
		if err != nil {
			return nil, fmt.Errorf("rows.Scan: %w", err)
		}

		var res outboxer.Task
		t.mapToModel(&res)

		tasks = append(tasks, &res)
	}

	err = rows.Err()
	if err != nil {
		return nil, fmt.Errorf("rows.Err: %w", err)
	}

	return tasks, nil
}
