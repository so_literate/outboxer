package postgresql

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"gitlab.com/so_literate/outboxer"
)

// ErrInvalidDataType returns when database returns wrong type for data.
var ErrInvalidDataType = errors.New("invalid data type")

type jsonb json.RawMessage

// Value returns driver.Value representation of the json.
// If value is nil, then returns SQL NULL. Doesn't check if JSON is valid.
func (j jsonb) Value() (driver.Value, error) {
	if j == nil {
		return nil, nil
	}

	return string(j), nil
}

// Scan stores the src in *j without json validation.
func (j *jsonb) Scan(src interface{}) error {
	switch t := src.(type) {
	case []byte:
		*j = append((*j)[0:0], t...)

	case nil:
		*j = nil

	default:
		return fmt.Errorf("%w: worng for JSONB type: %T(%#v)", ErrInvalidDataType, t, t)
	}

	return nil
}

type duration struct {
	dur *time.Duration
}

// Value returns driver.Value representation of the time.Duration in String format.
func (d duration) Value() (driver.Value, error) {
	if d.dur == nil {
		return nil, nil
	}

	return d.dur.String(), nil
}

// Scan parse string representation of the time.Duration and puts it into dur field.
func (d *duration) Scan(src interface{}) error {
	switch t := src.(type) {
	case string:
		dur, err := time.ParseDuration(t)
		if err != nil {
			return fmt.Errorf("time.ParseDuration: %w", err)
		}

		*d = duration{dur: &dur}

	case nil:
		*d = duration{}

	default:
		return fmt.Errorf("%w: worng for duration type: %T(%#v)", ErrInvalidDataType, t, t)
	}

	return nil
}

type task struct {
	id             int64
	createdAt      time.Time
	updatedAt      time.Time
	handler        string
	startAfter     time.Time
	inWorkAt       *time.Time
	doneAt         *time.Time
	doneWithFailAt *time.Time
	lastError      *string
	willClear      duration
	clearAfter     *time.Time
	repeatOnFail   duration
	repeatTask     duration
	data           jsonb
	onlyOneActive  bool
}

func (t *task) mapToModel(dst *outboxer.Task) {
	if t.id == 0 {
		dst.ID = ""
	} else {
		dst.ID = strconv.FormatInt(t.id, 10)
	}

	dst.CreatedAt = t.createdAt
	dst.UpdatedAt = t.updatedAt
	dst.Handler = t.handler
	dst.StartAfter = t.startAfter
	dst.InWorkAt = t.inWorkAt
	dst.DoneAt = t.doneAt
	dst.DoneWithFailAt = t.doneWithFailAt
	dst.LastError = t.lastError
	dst.WillClear = t.willClear.dur
	dst.ClearAfter = t.clearAfter
	dst.RepeatOnFail = t.repeatOnFail.dur
	dst.RepeatTask = t.repeatTask.dur
	dst.Data = json.RawMessage(t.data)
	dst.OnlyOneActive = t.onlyOneActive
}

func newTask(mt *outboxer.Task) (*task, error) {
	t := &task{
		createdAt:      mt.CreatedAt,
		updatedAt:      mt.UpdatedAt,
		handler:        mt.Handler,
		startAfter:     mt.StartAfter,
		inWorkAt:       mt.InWorkAt,
		doneAt:         mt.DoneAt,
		doneWithFailAt: mt.DoneWithFailAt,
		lastError:      mt.LastError,
		willClear:      duration{dur: mt.WillClear},
		clearAfter:     mt.ClearAfter,
		repeatOnFail:   duration{dur: mt.RepeatOnFail},
		repeatTask:     duration{dur: mt.RepeatTask},
		data:           jsonb(mt.Data),
		onlyOneActive:  mt.OnlyOneActive,
	}

	if mt.ID != "" {
		id, err := strconv.ParseInt(mt.ID, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("failed to parse task ID: %w", err)
		}

		t.id = id
	}

	return t, nil
}

func (t *task) getInsertColumns() []interface{} {
	return []interface{}{
		t.createdAt, t.updatedAt, t.handler, t.startAfter,
		t.inWorkAt, t.doneAt, t.doneWithFailAt, t.lastError,
		t.willClear, t.clearAfter, t.repeatOnFail, t.repeatTask,
		t.data, t.onlyOneActive,
	}
}

func (t *task) getUpdateColumns() []interface{} {
	return []interface{}{
		t.createdAt, t.updatedAt, t.handler, t.startAfter,
		t.inWorkAt, t.doneAt, t.doneWithFailAt, t.lastError,
		t.willClear, t.clearAfter, t.repeatOnFail, t.repeatTask,
		t.data, t.onlyOneActive, t.id,
	}
}
