package postgresql

import (
	"errors"
	"fmt"
	"regexp"
)

// ErrConfigFieldIsInvalid returns when one of the config's field is invalid.
var ErrConfigFieldIsInvalid = errors.New("config field is invalid")

var validConfigField = regexp.MustCompile(`^[A-z]+$`)

// Config is a configuration of the postgresql queries.
type Config struct {
	Schema    string
	TableName string
}

func newConfig() *Config {
	return &Config{
		Schema:    "public",
		TableName: "outboxer_tasks",
	}
}

// ConfigSetter sets some parameters to config.
type ConfigSetter func(conf *Config)

// ConfigWithSchema sets special schema to the Config.
// Schema with tables in database, "public" by default.
func ConfigWithSchema(schema string) ConfigSetter {
	return func(conf *Config) { conf.Schema = schema }
}

// ConfigWithTableName setes table name to the Config.
// TableName is a name of the table with tasks, "outboxer_tasks" by default.
func ConfigWithTableName(tableName string) ConfigSetter {
	return func(conf *Config) { conf.TableName = tableName }
}

func (conf *Config) runSetters(opts []ConfigSetter) *Config {
	for _, opt := range opts {
		opt(conf)
	}

	return conf
}

func (conf *Config) checkIsValid() (*Config, error) {
	if !validConfigField.MatchString(conf.Schema) {
		return nil, fmt.Errorf("%w: 'schema' %q", ErrConfigFieldIsInvalid, conf.Schema)
	}

	if !validConfigField.MatchString(conf.TableName) {
		return nil, fmt.Errorf("%w: 'tableName' %q", ErrConfigFieldIsInvalid, conf.TableName)
	}

	return conf, nil
}
