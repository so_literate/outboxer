// Package query contains all SQL queries of the storage.
// It was created to separate SQL from Storage logic.
package query

import (
	"fmt"
	"strings"
)

// Query contains all queries of the Storage.
type Query struct {
	schema, tableName, schemaAndTable string

	columnList             string
	columnListWithoutID    string
	columnReplaceWithoutID string

	CheckTableExists string
	Migrate          string

	GetTaskForWork string
	CreateTask     string
	SaveTask       string

	ClearTasks      string
	GetProblemTasks string
	GetStuckTasks   string
}

// New returns new Query object with compiled queries.
func New(schema, tableName string) *Query {
	q := &Query{
		schema:         schema,
		tableName:      tableName,
		schemaAndTable: fmt.Sprintf("%q.%q", schema, tableName),
	}

	return q.setQueries()
}

func (q *Query) setQueries() *Query {
	return q.
		setCheckTableExists().
		setMigrate().
		setGetTaskForWork().
		setCreateTask().
		setSaveTask().
		setClearTask().
		setGetProblemTasks().
		setGetStuckTasks()
}

func (q *Query) setCheckTableExists() *Query {
	q.CheckTableExists = fmt.Sprintf(`SELECT EXISTS(
	SELECT * FROM "information_schema"."tables"
	WHERE "table_schema" = '%s' AND "table_name" = '%s' AND "table_type" = 'BASE TABLE'
);`,
		q.schema, q.tableName,
	)

	return q
}

func (q *Query) setMigrate() *Query {
	q.Migrate = fmt.Sprintf(`
	CREATE SCHEMA IF NOT EXISTS %[1]s;

	CREATE TABLE %[2]s (
		"id"         BIGSERIAL,
  		"created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  		"updated_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),

		"handler"     TEXT        NOT NULL,
		"start_after" TIMESTAMPTZ NOT NULL,

		"in_work_at"        TIMESTAMPTZ,
		"done_at"           TIMESTAMPTZ,
		"done_with_fail_at" TIMESTAMPTZ,
		"last_error"        TEXT,

		"will_clear"  TEXT,
		"clear_after" TIMESTAMPTZ,

		"repeat_on_fail" TEXT,
		"repeat_task"    TEXT,

		"data"            JSONB,
		"only_one_active" BOOLEAN,

		PRIMARY KEY ("id")
	);

	CREATE INDEX "%[3]s_idx_ready_to_work" ON %[2]s ("in_work_at", "start_after");

	CREATE INDEX "%[3]s_idx_for_cleaner" ON %[2]s ("clear_after");

	CREATE INDEX "%[3]s_idx_stuck_tasks" ON %[2]s ("done_at", "done_with_fail_at", "in_work_at")
		WHERE "done_at" IS NULL AND "done_with_fail_at" IS NULL;

	CREATE UNIQUE INDEX "%[3]s_idx_only_one_active_handler" ON %[2]s ("handler")
		WHERE "only_one_active" = TRUE
		AND   ("done_at" IS NULL AND "done_with_fail_at" IS NULL);`,
		q.schema, q.schemaAndTable, q.tableName,
	)

	q.columnList = `"id", "created_at", "updated_at", "handler", "start_after",
	"in_work_at", "done_at", "done_with_fail_at", "last_error",
	"will_clear", "clear_after", "repeat_on_fail", "repeat_task",
	"data", "only_one_active"`

	q.columnListWithoutID = strings.TrimPrefix(q.columnList, `"id", `)

	n := strings.Count(q.columnList, ",") // number of ',' is equals to number of the fields but without 'id'.
	replaces := make([]string, n)

	for i := 0; i < n; i++ {
		replaces[i] = fmt.Sprintf("$%d", i+1)
	}

	q.columnReplaceWithoutID = strings.Join(replaces, ", ")

	return q
}

func (q *Query) setGetTaskForWork() *Query {
	q.GetTaskForWork = fmt.Sprintf(`UPDATE %[1]s
	SET in_work_at = $1, "updated_at" = $1
	WHERE "id" = (
		SELECT "id" FROM %[1]s
		WHERE "in_work_at" IS NULL AND "start_after" < $1
		ORDER BY "id" ASC
		LIMIT 1
		FOR UPDATE SKIP LOCKED
	)
	RETURNING %[2]s;`,
		q.schemaAndTable, q.columnList)

	return q
}

func (q *Query) setCreateTask() *Query {
	q.CreateTask = fmt.Sprintf(`INSERT INTO %s
	(
	%s
	)
	VALUES (%s)
	ON CONFLICT DO NOTHING
	RETURNING "id";`,
		q.schemaAndTable, q.columnListWithoutID, q.columnReplaceWithoutID)

	return q
}

func (q *Query) setSaveTask() *Query {
	q.SaveTask = fmt.Sprintf(`UPDATE %s SET
	"created_at" = $1, "updated_at" = $2, "handler" = $3, "start_after" = $4,
	"in_work_at" = $5, "done_at" = $6, "done_with_fail_at" = $7, "last_error" = $8,
	"will_clear" = $9, "clear_after" = $10, "repeat_on_fail" = $11, "repeat_task" = $12,
	"data" = $13, "only_one_active" = $14
WHERE "id" = $15;`,
		q.schemaAndTable)

	return q
}

func (q *Query) setClearTask() *Query {
	q.ClearTasks = fmt.Sprintf(`DELETE FROM %s WHERE "clear_after" < $1;`, q.schemaAndTable)

	return q
}

func (q *Query) setGetProblemTasks() *Query {
	q.GetProblemTasks = fmt.Sprintf(`SELECT
	(SELECT count(1) FROM %[1]s WHERE "done_at" IS NULL AND "done_with_fail_at" IS NULL AND "in_work_at" < $1) AS "stuck",
	(SELECT count(1) FROM %[1]s WHERE "done_with_fail_at" IS NOT NULL) AS "failed";`,
		q.schemaAndTable)

	return q
}

func (q *Query) setGetStuckTasks() *Query {
	q.GetStuckTasks = fmt.Sprintf(
		`SELECT
	%s
	FROM %s
	WHERE "done_at" IS NULL AND "done_with_fail_at" IS NULL AND "in_work_at" < $1
	LIMIT 100`,
		q.columnList, q.schemaAndTable)

	return q
}
