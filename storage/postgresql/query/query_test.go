package query_test

import (
	"testing"

	"gitlab.com/so_literate/outboxer/storage/postgresql/query"
)

func TestQuery(t *testing.T) {
	t.Parallel()

	assert := func(gotQuery, wantQuery string) {
		t.Helper()

		if gotQuery != wantQuery {
			t.Errorf("not equal:\n%q\n%q", gotQuery, wantQuery)
		}
	}

	q := query.New("public", "outboxer_tasks")

	assert(
		q.CheckTableExists,
		`SELECT EXISTS(
	SELECT * FROM "information_schema"."tables"
	WHERE "table_schema" = 'public' AND "table_name" = 'outboxer_tasks' AND "table_type" = 'BASE TABLE'
);`,
	)

	assert(
		q.ClearTasks,
		`DELETE FROM "public"."outboxer_tasks" WHERE "clear_after" < $1;`,
	)

	assert(
		q.CreateTask,
		`INSERT INTO "public"."outboxer_tasks"
	(
	"created_at", "updated_at", "handler", "start_after",
	"in_work_at", "done_at", "done_with_fail_at", "last_error",
	"will_clear", "clear_after", "repeat_on_fail", "repeat_task",
	"data", "only_one_active"
	)
	VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)
	ON CONFLICT DO NOTHING
	RETURNING "id";`,
	)

	assert(
		q.GetProblemTasks,
		`SELECT
	(SELECT count(1) FROM "public"."outboxer_tasks" WHERE "done_at" IS NULL AND "done_with_fail_at" IS NULL AND "in_work_at" < $1) AS "stuck",
	(SELECT count(1) FROM "public"."outboxer_tasks" WHERE "done_with_fail_at" IS NOT NULL) AS "failed";`,
	)

	assert(
		q.GetStuckTasks,
		`SELECT
	"id", "created_at", "updated_at", "handler", "start_after",
	"in_work_at", "done_at", "done_with_fail_at", "last_error",
	"will_clear", "clear_after", "repeat_on_fail", "repeat_task",
	"data", "only_one_active"
	FROM "public"."outboxer_tasks"
	WHERE "done_at" IS NULL AND "done_with_fail_at" IS NULL AND "in_work_at" < $1
	LIMIT 100`,
	)

	assert(
		q.GetTaskForWork,
		`UPDATE "public"."outboxer_tasks"
	SET in_work_at = $1, "updated_at" = $1
	WHERE "id" = (
		SELECT "id" FROM "public"."outboxer_tasks"
		WHERE "in_work_at" IS NULL AND "start_after" < $1
		ORDER BY "id" ASC
		LIMIT 1
		FOR UPDATE SKIP LOCKED
	)
	RETURNING "id", "created_at", "updated_at", "handler", "start_after",
	"in_work_at", "done_at", "done_with_fail_at", "last_error",
	"will_clear", "clear_after", "repeat_on_fail", "repeat_task",
	"data", "only_one_active";`,
	)

	assert(
		q.Migrate,
		`
	CREATE SCHEMA IF NOT EXISTS public;

	CREATE TABLE "public"."outboxer_tasks" (
		"id"         BIGSERIAL,
  		"created_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  		"updated_at" TIMESTAMPTZ NOT NULL DEFAULT NOW(),

		"handler"     TEXT        NOT NULL,
		"start_after" TIMESTAMPTZ NOT NULL,

		"in_work_at"        TIMESTAMPTZ,
		"done_at"           TIMESTAMPTZ,
		"done_with_fail_at" TIMESTAMPTZ,
		"last_error"        TEXT,

		"will_clear"  TEXT,
		"clear_after" TIMESTAMPTZ,

		"repeat_on_fail" TEXT,
		"repeat_task"    TEXT,

		"data"            JSONB,
		"only_one_active" BOOLEAN,

		PRIMARY KEY ("id")
	);

	CREATE INDEX "outboxer_tasks_idx_ready_to_work" ON "public"."outboxer_tasks" ("in_work_at", "start_after");

	CREATE INDEX "outboxer_tasks_idx_for_cleaner" ON "public"."outboxer_tasks" ("clear_after");

	CREATE INDEX "outboxer_tasks_idx_stuck_tasks" ON "public"."outboxer_tasks" ("done_at", "done_with_fail_at", "in_work_at")
		WHERE "done_at" IS NULL AND "done_with_fail_at" IS NULL;

	CREATE UNIQUE INDEX "outboxer_tasks_idx_only_one_active_handler" ON "public"."outboxer_tasks" ("handler")
		WHERE "only_one_active" = TRUE
		AND   ("done_at" IS NULL AND "done_with_fail_at" IS NULL);`,
	)

	assert(
		q.SaveTask,
		`UPDATE "public"."outboxer_tasks" SET
	"created_at" = $1, "updated_at" = $2, "handler" = $3, "start_after" = $4,
	"in_work_at" = $5, "done_at" = $6, "done_with_fail_at" = $7, "last_error" = $8,
	"will_clear" = $9, "clear_after" = $10, "repeat_on_fail" = $11, "repeat_task" = $12,
	"data" = $13, "only_one_active" = $14
WHERE "id" = $15;`,
	)
}
