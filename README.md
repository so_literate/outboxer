OutBoxer
========

Outbox pattern helper.

# Features

Ouboxer's runners periodically ask for tasks and complete it.

- Flexible task settings.
- Simple flow of the runner.
- Provides the creation of a task in a transaction.
- Deps free.

# Example:

(see full example in `_examples` directory)

```go
package main


import (
	"log"
	"time"

	"gitlab.com/so_literate/outboxer"
)

func main() {
	obx := outboxer.New(storage) // postgresql for example
	obx.RegisterHandler("task", taskHandler)

	// Will create task after run command.
	go createTask(obx)

	if err := obx.Run(context.Background()); err != nil {
		log.Fatal(err)
	}
}

func createTask(obx *outboxer.Outboxer) {
	time.Sleep(time.Second)

	data := &taskData{JSONField: "data of the task"}

	task := outboxer.NewTaskWithObject(handlerName, time.Now(), data).
		WillBeRepeated(time.Second).
		WillBeRepeatedOnFail(time.Second)

	if err := obx.CreateTask(context.Background(), task); err != nil {
		log.Fatal("obx.CreateTask: ", err)
	}
}

type taskData struct {
	JSONField string
}

func taskHandler(ctx context.Context, task *outboxer.Task) error {
	var data taskData

	if err := task.UnmarshalData(&data); err != nil {
		return fmt.Errorf("task.UnmarshalData: %w", err)
	}

	log.Print("task handler: ", data.JSONField)

	return nil
}
```
