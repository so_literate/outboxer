package outboxer

import (
	"context"
	"errors"
	"fmt"
	"time"
)

func (o *Outboxer) tryToRunTask(ctx context.Context, task *Task) error {
	o.handlersMu.RLock()
	handler, ok := o.handlers[task.Handler]
	o.handlersMu.RUnlock()

	if !ok {
		return fmt.Errorf("%w: %q", ErrHandlerNotFound, task.Handler)
	}

	for _, mw := range o.config.Middlewares {
		handler = mw(handler)
	}

	return handler(ctx, task)
}

func (o *Outboxer) setTaskWithError(task *Task, err error) {
	errText := err.Error()
	now := time.Now()

	task.DoneAt = nil
	task.LastError = &errText
	task.DoneWithFailAt = &now

	if task.RepeatOnFail != nil {
		task.DoneWithFailAt = nil
		task.InWorkAt = nil
		task.StartAfter = task.StartAfter.Add(*task.RepeatOnFail)
	}
}

func (o *Outboxer) setTaskWithSuccess(task *Task) {
	now := time.Now()

	task.DoneAt = &now
	task.DoneWithFailAt = nil
	task.LastError = nil

	if task.WillClear != nil {
		clearAfter := now.Add(*task.WillClear)
		task.ClearAfter = &clearAfter
	}

	if task.RepeatTask != nil {
		task.DoneAt = nil
		task.InWorkAt = nil
		task.ClearAfter = nil

		// Task must start in the feature to avoid extra runnings.
		for task.StartAfter.Before(now) {
			task.StartAfter = task.StartAfter.Add(*task.RepeatTask)
		}
	}
}

func (o *Outboxer) runTask(ctx context.Context, task *Task) error {
	// Create a new ctx without cancel propagate feature.
	// We need it to Save task result.
	noPropagateCtx := NewContextWithoutCancel(ctx)

	// But for task we create a special one with custom cancel func
	// to cancel it if we reach shutdown deadline.
	taskCtx, cancelTaskCtx := context.WithCancel(noPropagateCtx)

	defer cancelTaskCtx()

	taskResult := make(chan error, 1)

	go func() {
		taskResult <- o.tryToRunTask(taskCtx, task)
	}()

	var err error

	select {
	// the main context is canceled, waiting for the result or graceful shutdown deadline.
	case <-ctx.Done():
		deadline := time.NewTimer(o.config.GracefulTimeout)

		select {
		// it was enough time for the task.
		case <-deadline.C:
			// save result as an error and cancel its context in defer function.
			err = fmt.Errorf("%w: GracefulTimeout reached the deadline for task", context.Canceled)

		// got the task result.
		case err = <-taskResult:
			// need to stop the timer and save result.
			if !deadline.Stop() {
				<-deadline.C
			}
		}

	// got result from the task, just save result of the task.
	case err = <-taskResult:
	}

	if err != nil {
		o.setTaskWithError(task, err)
	} else {
		o.setTaskWithSuccess(task)
	}

	err = o.Storage.SaveTask(noPropagateCtx, task)
	if err != nil {
		return fmt.Errorf("failed to save task %s(%s): %w", task.Handler, task.ID, err)
	}

	return nil
}

func (o *Outboxer) goToWork(ctx context.Context) (noJob bool) {
	task, err := o.Storage.GetTaskForWork(ctx, time.Now())
	if err != nil {
		if !errors.Is(err, ErrTaskNotFound) {
			o.logf("failed to get task: %s", err)
		}

		return true
	}

	if err = o.runTask(ctx, task); err != nil {
		o.logf("failed to run task: %s", err)
		return true
	}

	return false
}
