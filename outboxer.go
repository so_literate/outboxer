// Package outboxer is a helper to implement outbox pattern in Go.
// It contains background runners and transactional access to storage.
package outboxer

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"sync"
	"time"
)

// Storage is an access to persistent storage with outbox messages.
//
//go:generate genmock -search.name=Storage -print.place.in_package -print.package.test -print.file.test
type Storage interface {
	// Migrate will be called in run method to create default schema in the storage.
	Migrate(ctx context.Context) error

	// GetTaskForWork returns non active task on the current time.
	GetTaskForWork(ctx context.Context, now time.Time) (*Task, error)

	// CreateTask creates a new task with, fields: ID, created_at, updated_at will be set.
	CreateTask(ctx context.Context, task *Task) error

	// CreateTaskWithTx creates a new task in transaction.
	CreateTaskWithTx(ctx context.Context, tx interface{}, task *Task) error

	// SaveTask saves all changes of the task.
	SaveTask(ctx context.Context, task *Task) error

	// ClearTasks clears all tasks with mark 'ClearAfter'.
	ClearTasks(ctx context.Context, now time.Time) error

	// GetProblemTasks returns number of the stuck or failed tasks without repeat.
	GetProblemTasks(ctx context.Context, inWorkOlderThan time.Time) (stuck, failed int64, err error)

	// GetStuckTasks returns stuck tasks from storage used by restarter.
	GetStuckTasks(ctx context.Context, inWorkOlderThan time.Time) ([]*Task, error)
}

// Handler is a task handler of the outboxer job.
// Takes context from Run method and executable task.
// The error of the handler will be saved to task in the storage.
type Handler func(ctx context.Context, task *Task) error

// Middleware is a custom handler that will be called before task handler.
// You can use it to log errors or enrich the context.
type Middleware func(next Handler) Handler

// Outboxer main object of the package has methods to run and control helper.
type Outboxer struct {
	Logger  *log.Logger
	Storage Storage

	config     *Config
	handlersMu sync.RWMutex
	handlers   map[string]Handler
}

// New returns new Outboxer object with runners.
// Takes storage and optional config.
func New(storage Storage, configOptions ...ConfigOption) *Outboxer {
	conf := NewConfig(configOptions...)

	res := &Outboxer{
		Logger:  nil,
		Storage: storage,

		config:   conf,
		handlers: make(map[string]Handler, 2),
	}

	if conf.ErrorLogger != nil {
		res.Logger = log.New(conf.ErrorLogger, "[outboxer] ", 0)
	}

	return res
}

func (o *Outboxer) logf(format string, v ...interface{}) {
	if o.Logger == nil {
		return
	}

	o.Logger.Printf(format+"\n", v...)
}

// Run runs background workers and wait stop signal in context.
// It takes context to stop outboxer.
func (o *Outboxer) Run(ctx context.Context) (err error) {
	if o.config.PreRunFunc != nil {
		ctx = o.config.PreRunFunc(ctx)
	}

	if err := o.Storage.Migrate(ctx); err != nil {
		err = fmt.Errorf("storage.Migrate: %w", err)
		return err
	}

	if err := o.cleanerCreateTask(ctx); err != nil {
		err = fmt.Errorf("failed to create cleaner task: %w", err)
		return err
	}

	if err := o.restarterCreateTask(ctx); err != nil {
		err = fmt.Errorf("failed to create cleaner task: %w", err)
		return err
	}

	var wg sync.WaitGroup
	wg.Add(o.config.RunnersNumber)

	for i := 0; i < o.config.RunnersNumber; i++ {
		go func() {
			o.startRunner(ctx)
			wg.Done()
		}()
	}

	wg.Wait()
	return nil
}

// RegisterHandler register handler of the outboxer task.
func (o *Outboxer) RegisterHandler(handlerName string, handler Handler) {
	o.handlersMu.Lock()
	o.handlers[handlerName] = handler
	o.handlersMu.Unlock()
}

// NewTask creates the task object without saving in the storage.
// It will be called panic if you pass bad type for json.Marshal func in object argument.
func (*Outboxer) NewTask(handler string, startAfter time.Time, object interface{}) *Task {
	return NewTaskWithObject(handler, startAfter, object)
}

// CreateTask saved a new task in the storage.
func (o *Outboxer) CreateTask(ctx context.Context, task *Task) error {
	return o.Storage.CreateTask(ctx, task)
}

// CreateTaskWithTx saved a new task in the storage with transaction.
// The tx must be supported type of the storage.
// See examples in postgresql storage, it supports std *sql.Tx
// or RowQuerier interface for Gorm and other ORMs.
func (o *Outboxer) CreateTaskWithTx(ctx context.Context, tx interface{}, task *Task) error {
	return o.Storage.CreateTaskWithTx(ctx, tx, task)
}

// CreateTaskWithTxSQL saved a new task in the storage with SQL transaction.
func (o *Outboxer) CreateTaskWithTxSQL(ctx context.Context, tx *sql.Tx, task *Task) error {
	return o.Storage.CreateTaskWithTx(ctx, tx, task)
}

// GetProblemTasksAmount returns amount of the problem tasks in the Storage.
// Problem task is a task with mark DoneWithFailAt or
// a task with mark InWorkAt without any Done mark longer than StuckTaskAfter parameter.
func (o *Outboxer) GetProblemTasksAmount(ctx context.Context) (stuck, failed int64, err error) {
	return o.Storage.GetProblemTasks(ctx, time.Now().Add(-o.config.StuckTaskAfter))
}

// RestartStuckTasks restarts stuck tasks in the Storage.
// Outboxer has a task to do it automatically.
func (o *Outboxer) RestartStuckTasks(ctx context.Context) error {
	return o.restarterHandler(ctx, &Task{})
}

// gotStopSignal runs deadline timeout or waits job's result.
func (o *Outboxer) gotStopSignal(jobResult <-chan bool) {
	deadline := time.NewTimer(o.config.GracefulShutdownTimeout)

	select {
	// Got deadline of the timeout.
	case <-deadline.C:
		o.logf("GracefulShutdownTimeout reached the deadline")

	// Got result of the job.
	case <-jobResult:
		if !deadline.Stop() {
			<-deadline.C
		}
	}
}

func (o *Outboxer) startRunner(ctx context.Context) {
	jobResult := make(chan bool)

	timeToWork := time.NewTimer(o.config.NextJobTimeout)

	mustStopTimer := true

	defer func() {
		if mustStopTimer && !timeToWork.Stop() {
			<-timeToWork.C
		}
	}()

	for {
		select {
		// Got cancel signal before job was started, just stop and go out.
		case <-ctx.Done():
			return

		// Got signal to start work.
		case <-timeToWork.C:
			mustStopTimer = false
		}

		go func() {
			jobResult <- o.goToWork(ctx)
		}()

		select {
		// Got cancel signal from parent launcher, BUT job was already started.
		case <-ctx.Done():
			o.gotStopSignal(jobResult)
			return

		// Got result of the work.
		case noJob := <-jobResult:
			if noJob {
				timeToWork.Reset(o.config.NextJobTimeout) // no job here, wait config time.
			} else {
				timeToWork.Reset(0) // runner must to start the next job right now.
			}

			mustStopTimer = true
		}
	}
}
