package outboxer_test

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/so_literate/outboxer"
)

func TestTask(t *testing.T) {
	t.Parallel()

	assertTask := func(want, got *outboxer.Task) {
		t.Helper()

		if !reflect.DeepEqual(want, got) {
			t.Errorf("not equals:\n%#v\n%#v", want, got)
		}
	}

	taskHandlerName := "handler"
	taskStartAfter := time.Date(2022, 4, 30, 12, 24, 59, 123, time.UTC)
	dur := time.Second

	wantTask := &outboxer.Task{
		Handler:    taskHandlerName,
		StartAfter: taskStartAfter,
	}

	assertTask(
		wantTask,
		outboxer.NewTaskWithObject(taskHandlerName, taskStartAfter, nil),
	)

	wantTask.Data = []byte(`"text"`)
	assertTask(
		wantTask,
		outboxer.NewTaskWithObject(taskHandlerName, taskStartAfter, "text"),
	)

	wantTask.WillClear = &dur
	wantTask.RepeatTask = &dur
	wantTask.RepeatOnFail = &dur
	wantTask.OnlyOneActive = true

	assertTask(
		wantTask,
		outboxer.NewTaskWithObject(taskHandlerName, taskStartAfter, "text").
			WillBeCleared(dur).
			WillBeRepeated(dur).
			WillBeRepeatedOnFail(dur).
			WillBeOnlyOneActive(),
	)

	var str string
	err := wantTask.UnmarshalData(&str)
	if err != nil {
		t.Fatal(err)
	}

	if str != "text" {
		t.Fatal("wrong result", str)
	}

	var wasPanic bool

	doTest := func() {
		defer func() {
			if rec := recover(); rec != nil {
				wasPanic = true
			}
		}()

		outboxer.NewTaskWithObject(taskHandlerName, taskStartAfter, make(chan int))
	}

	doTest()
	if !wasPanic {
		t.Fatal("not panic")
	}
}
